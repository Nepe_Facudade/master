﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Torneio.Web.Core
{
    public static class Extensions
    {
        public static string GetHeaderValue(this HttpRequest request, string headerName)
        {
            IEnumerable<string> values = null;

            if (!request.TryGetHeaderValues(headerName, out values))
            {
                return null;
            }

            return values.FirstOrDefault();
        }

        public static bool TryGetHeaderValues(this HttpRequest request, string headerName, out IEnumerable<string> values)
        {
            if (request.Headers.ContainsKey(headerName))
            {
                values = request.Headers[headerName];
                return true;
            }
            else
            {
                values = null;
                return false;
            }
        }

        public static Dictionary<string, string> GetQueryNameValuePairs(this HttpRequest request)
        {
            Dictionary<string, string> queryStringPairs = new Dictionary<string, string>();

            if (!ReferenceEquals(request.Query, null) && request.Query.Count > 0)
            {
                foreach (KeyValuePair<string, StringValues> item in request.Query)
                {
                    queryStringPairs[item.Key] = item.Value.FirstOrDefault();
                }
            }

            return queryStringPairs;
        }

        public static Dictionary<string, string> GetFormData(this HttpRequest request)
        {
            Dictionary<string, string> formData = new Dictionary<string, string>();

            if (!ReferenceEquals(request.Form, null) && request.Form.Count > 0)
            {
                foreach (KeyValuePair<string, StringValues> item in request.Form)
                {
                    formData[item.Key] = item.Value.FirstOrDefault();
                }
            }

            return formData;
        }

        public static async Task<string> ReadContentAsStringAsync(this HttpRequest request)
        {
            string contentString = null;

            using (StreamReader reader = new StreamReader(request.Body, Encoding.UTF8))
            {
                contentString = await reader.ReadToEndAsync();
            }

            return contentString;
        }
         
        public static byte[] ReadContentAsByteArray(this HttpRequest request)
        {
            int byteRead = 0;
            int position = 0;

            byte[] contentBytes = new byte[request.Body.Length];

            request.Body.Position = position;

            while ((byteRead = request.Body.ReadByte()) >= 0)
            {
                contentBytes[position] = (byte)byteRead;
                position++;
            }

            return contentBytes;
        }

        public static bool IsSuccessStatusCode(this ContentResult result)
        {
            switch (result.StatusCode ?? 0)
            {
                case 200:
                case 201:
                case 202:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsNull(this JObject json, string propertyName)
        {
            if (ReferenceEquals(json, null) || ReferenceEquals(json[propertyName], null) || json[propertyName].Type == JTokenType.Null) { return true; }

            return false;
        }
    }
}

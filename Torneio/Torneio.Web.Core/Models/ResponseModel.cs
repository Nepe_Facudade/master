﻿using Newtonsoft.Json;

namespace Torneio.Web.Core.Models
{
    public partial class Fighters
    {
        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("nome")]
        public string nome { get; set; }

        [JsonProperty("idade")]
        public int idade { get; set; }

        [JsonProperty("artesMarciais")]
        public string[] artesMarciais { get; set; }

        [JsonProperty("lutas")]
        public int lutas { get; set; }

        [JsonProperty("derrotas")]
        public int derrotas { get; set; }

        [JsonProperty("vitorias")]
        public int vitorias { get; set; }

        [JsonProperty("porcentagem")]
        public int porcentagem { get; set; }
        
    } 

}
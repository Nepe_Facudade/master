﻿
using System;

namespace Torneio.Web.Core.Interfaces
{
    public interface ILog
    {
        void Error(string message, Exception error = null);
        void Info(string message);
        void Warning(string message);
    }
}
﻿
using System;

namespace Torneio.Web.Core.Interfaces
{ 
        public interface IAuthenticator
        {
            Guid Identity { get; }
            Guid Credential { get; }
            string IdentityName { get; }
            Guid PartitionKey { get; }
            int Privilege { get; }
            int Role { get; } 

            void Assign(IAuthenticable authenticable);
        }
    }
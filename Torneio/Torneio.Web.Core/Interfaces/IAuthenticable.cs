﻿
using System;

namespace Torneio.Web.Core.Interfaces
{
    public interface IAuthenticable : IAuthenticator
    {
        int permission { get; set; }
        string createdByName { get; set; }
        Guid? createdBy { get; set; }
        DateTime? createdOn { get; set; }
        string updatedByName { get; set; }
        Guid? updatedBy { get; set; }
        DateTime? lastUpdate { get; set; }
        string excludedByName { get; set; }
        Guid? excludedBy { get; set; }
        DateTime? excludedOn { get; set; }

        void Authenticate(Guid identity, string identityName, Guid currentScope, Guid credential, int privilege, int role);
    }
}
﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security;
using System.Threading.Tasks;

namespace Torneio.Web.Core.Http
{
    using Core.Interfaces; 

    public static class Proxy
    {
        static Proxy()
        {
            Application.Start();
        }

        public delegate object ApiFunctionDelegate(JsonEnvelope envelope, out bool applyEnvelope);

        public static async Task<ContentResult> ResolveJSON(HttpRequest request, ILog log, bool requiresAuth, ApiFunctionDelegate apiFunction, params string[] routeParams)
        {
            string jsonString = null;
            JsonEnvelope envelope = null;
            object result = null;

            bool applyEnvelope = false;

            string message = null;
            WarningLevel warningLevel = WarningLevel.Info;
            int? pageSize = null;
            int? pageIndex = null;
            int? pageCount = null;
            int? totalCount = null;

            IEnumerable<KeyValuePair<string, string>> parameters = null;
             
            ContentResult response = null;
            HttpStatusCode errorStatusCode = HttpStatusCode.InternalServerError;

            try 
            {  
                if (!ReferenceEquals(request.Body, null))
                {
                    jsonString = await request.ReadContentAsStringAsync();
                    envelope = JsonEnvelope.Parse(jsonString);
                }

                if (ReferenceEquals(envelope, null))
                {
                    envelope = new JsonEnvelope();
                }

                if (!ReferenceEquals(routeParams, null) && routeParams.Any())
                {
                    envelope.AddRouteParameters(routeParams);
                }

                parameters = request.GetQueryNameValuePairs();

                if (!ReferenceEquals(parameters, null))
                {
                    foreach (KeyValuePair<string, string> item in parameters)
                    {
                        envelope.AddQueryStringParameter(item.Key, item.Value);
                    }
                }

                result = await Task.Run(() => apiFunction(envelope, out applyEnvelope));

                 
                if (applyEnvelope)
                {
                    message = envelope.Message;
                    warningLevel = envelope.WarningLevel;
                    pageSize = envelope.PageSize;
                    pageIndex = envelope.PageIndex;
                    pageCount = envelope.PageCount;
                    totalCount = envelope.TotalCount;

                    response = RespondEnveloped(log, result, envelope, request);
                }
                else // RAW RESPONSE
                {
                    response = RespondJSON(log, result, request);
                }
            }
            catch (SecurityException error)
            {
                if (!ReferenceEquals(error.PermissionType, null))
                {
                    // INVALID TOKEN OR SESSION EXPIRED = 401 (for client redirect)
                    response = ThrowHttpError(log, error.Message, request, HttpStatusCode.Unauthorized);
                }
                else
                {
                    // ANY OTHER SECURITY ERROR = 403
                    response = ThrowHttpError(log, error.Message, request, HttpStatusCode.Forbidden);
                }
            } 

            return response;
        }

        internal static ContentResult RespondJSON(ILog log, object data, HttpRequest request)
        {
            ContentResult result = CreateDefaultResponse(request);

            string jsonData = JsonConvert.SerializeObject(data);

            result.Content = jsonData;

            //log.Info(jsonData);

            return result;
        }

        private static ContentResult CreateDefaultResponse(HttpRequest request, string contentType)
        {
            return CreateDefaultResponse(request, HttpStatusCode.OK, contentType);
        }
        private static ContentResult CreateDefaultResponse(HttpRequest request, HttpStatusCode statusCode = HttpStatusCode.OK, string contentType = "application/json")
        {
            ContentResult result = new ContentResult();

            result.StatusCode = (int)statusCode;
            result.ContentType = contentType;

            // TODO: Apply default headers to response

            return result;
        }

        internal static ContentResult RespondEnveloped(ILog log, object data, JsonEnvelope envelope, HttpRequest request)
        {
            JsonEnvelope result = new JsonEnvelope(data, envelope.Message, envelope.WarningLevel);

            result.PageSize = envelope.PageSize;
            result.PageIndex = envelope.PageIndex;
            result.PageCount = envelope.PageCount;
            result.TotalCount = envelope.TotalCount;

            return RespondJSON(log, result, request);
        } 
        internal static ContentResult ThrowHttpError(ILog log, string message, HttpRequest request, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
        {
            ContentResult result = CreateDefaultResponse(request, statusCode, "text/plain");
            result.Content = message;
            log.Error(string.Concat("#ERROR# ", message));
            return result;
        }
    }
}

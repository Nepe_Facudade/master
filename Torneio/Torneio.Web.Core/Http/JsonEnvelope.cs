﻿ 
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Web;
using Torneio.Web.Core.Interfaces;

namespace Torneio.Web.Core.Http
{
    public class JsonEnvelope
    {
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("warningLevel", NullValueHandling = NullValueHandling.Ignore)]
        public WarningLevel WarningLevel { get; set; }
        [JsonProperty("content")]
        public object Content { get; set; }
        [JsonProperty("pageSize")]
        public int? PageSize { get; set; }
        [JsonProperty("pageIndex")]
        public int? PageIndex { get; set; }
        [JsonProperty("pageCount")]
        public int? PageCount { get; set; }
        [JsonProperty("totalCount")]
        public int? TotalCount { get; set; }

        private List<string> _routeParams { get; set; }
        private Dictionary<string, string> _queryStringParams { get; set; }

        private static readonly CultureInfo _envelopeCulture = new CultureInfo("en-US");
        private static readonly JsonSerializerSettings _envelopeSettings = new JsonSerializerSettings() { Culture = _envelopeCulture };

        public JsonEnvelope()
        {
        }

        public JsonEnvelope(object data, string message, WarningLevel warningLevel)
        {
            this.Message = message;
            this.WarningLevel = warningLevel;

            if (ReferenceEquals(data, null))
            {
                this.Content = null;
                return;
            }

            Type dataType = data.GetType();

            if (data is JObject || data is JArray || data is JToken)
            {
                this.Content = data;
            }
            else if (dataType.IsValueType || dataType.Equals(typeof(string)))
            {
                this.Content = data;
            }
            else if (dataType.IsArray || (dataType.IsGenericType && typeof(IEnumerable).IsAssignableFrom(dataType.GetGenericTypeDefinition())))
            {
                this.Content = JArray.FromObject(data);
            }
            else
            {
                this.Content = JObject.FromObject(data);
            }
        }

        public void Totalize(int totalCount, int limit, int skip)
        {
            this.PageSize = limit;
            this.PageIndex = (int)Math.Floor(((decimal)skip) / ((decimal)limit));
            this.PageCount = (int)Math.Ceiling(((decimal)totalCount) / ((decimal)limit));
            this.TotalCount = totalCount;
        }

        public Nullable<T> GetContentValue<T>(string propertyName) where T : struct
        {
            Nullable<T> result = null;

            if (ReferenceEquals(this.Content, null)) { return result; }

            JArray array = null;
            JObject obj = null;

            if (this.Content is JArray)
            {
                array = this.Content as JArray;
                foreach (JObject item in array)
                {
                    if (!ReferenceEquals(item[propertyName], null))
                    {
                        result = (T)item[propertyName].ToObject(typeof(T));
                        break;
                    }
                }
            }
            else if (this.Content is JObject)
            {
                obj = this.Content as JObject;
                if (!ReferenceEquals(obj[propertyName], null))
                {
                    result = (T)obj[propertyName].ToObject(typeof(T));
                }
            }

            return result;
        }

        public string GetContentValue(string propertyName)
        {
            string result = null;

            if (ReferenceEquals(this.Content, null)) { return result; }

            JArray array = null;
            JObject obj = null;

            if (this.Content is JArray)
            {
                array = this.Content as JArray;
                foreach (JObject item in array)
                {
                    if (!ReferenceEquals(item[propertyName], null))
                    {
                        result = item[propertyName].ToString();
                        break;
                    }
                }
            }
            else if (this.Content is JObject)
            {
                obj = this.Content as JObject;
                if (!ReferenceEquals(obj[propertyName], null))
                {
                    result = obj[propertyName].ToString();
                }
            }

            return result;
        }

        public T GetContentData<T>(string propertyName) where T : class
        {
            T result = null;

            if (ReferenceEquals(this.Content, null)) { return result; }

            JArray array = null;
            JObject obj = null;

            if (this.Content is JArray)
            {
                array = this.Content as JArray;
                foreach (JObject item in array)
                {
                    if (!ReferenceEquals(item[propertyName], null))
                    {
                        result = (T)item[propertyName].ToObject(typeof(T));
                        break;
                    }
                }
            }
            else if (this.Content is JObject)
            {
                obj = this.Content as JObject;
                if (!ReferenceEquals(obj[propertyName], null))
                {
                    result = (T)obj[propertyName].ToObject(typeof(T));
                }
            }

            return result;
        }

        public string GetRouteParameter(int index)
        {
            if (ReferenceEquals(_routeParams, null) || _routeParams.Count <= index) { return null; }
            return _routeParams[index];
        }

        public bool HasRouteParameter(string value)
        {
            if (ReferenceEquals(_routeParams, null)) { return false; }
            return _routeParams.Contains(value);
        }

        public void AddRouteParameters(params string[] values)
        {
            if (ReferenceEquals(_routeParams, null))
            {
                _routeParams = new List<string>();
            }

            _routeParams.AddRange(values);
        } 
        public void AddQueryStringParameter(string key, string value)
        {
            if (ReferenceEquals(_queryStringParams, null))
            {
                _queryStringParams = new Dictionary<string, string>();
            }

            _queryStringParams[key] = value;
        }

         
        public T ToObject<T>() where T : class 
        {
            T result = null;

            if (!ReferenceEquals(this.Content, null))
            {
                result = (this.Content as JObject).ToObject<T>(); 
            }

            return result;
        }

        public T ToJson<T>() where T : class
        {
            T result = null;

            if (!ReferenceEquals(this.Content, null))
            {
                result = JsonConvert.DeserializeObject<T>(this.Content.ToString());
            }

            return result;
        }

        public object ToObject(Type objectType, IAuthenticator auth)
        {
            object result = null;

            if (!ReferenceEquals(this.Content, null) && this.Content is JObject)
            {
                result = (this.Content as JObject).ToObject(objectType);

                if (!ReferenceEquals(auth, null) && typeof(IAuthenticable).IsAssignableFrom(objectType))
                {
                    auth.Assign(result as IAuthenticable);
                }
            }

            return result;
        }

        public static JsonEnvelope Parse(string json)
        {
            return JsonConvert.DeserializeObject<JsonEnvelope>(json, _envelopeSettings);
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, _envelopeSettings);
        }
    }
}

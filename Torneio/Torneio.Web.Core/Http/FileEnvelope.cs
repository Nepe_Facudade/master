﻿using System;
using System.Collections.Generic;

namespace Torneio.Web.Core.Http
{
    public class FileEnvelope
    {
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] Buffer { get; set; }

        private List<string> _routeParams { get; set; }
        private Dictionary<string, string> _queryStringParams { get; set; }

        public FileEnvelope()
        {
        }

        public FileEnvelope(string fileName, string contentType, byte[] buffer)
        {
        }

        public string GetRouteParameter(int index)
        {
            if (ReferenceEquals(_routeParams, null) || _routeParams.Count <= index) { return null; }
            return _routeParams[index];
        }

        public bool HasRouteParameter(string value)
        {
            if (ReferenceEquals(_routeParams, null)) { return false; }
            return _routeParams.Contains(value);
        }

        public void AddRouteParameters(params string[] values)
        {
            if (ReferenceEquals(_routeParams, null))
            {
                _routeParams = new List<string>();
            }

            _routeParams.AddRange(values);
        }

        public Nullable<T> GetQueryStringParameter<T>(string key) where T : struct
        {
            if (ReferenceEquals(_queryStringParams, null) || !_queryStringParams.ContainsKey(key) || _queryStringParams[key] == null) { return null; }
            return (T)Convert.ChangeType(_queryStringParams[key], typeof(T));
        }

        public string GetQueryStringParameter(string key)
        {
            if (ReferenceEquals(_queryStringParams, null) || !_queryStringParams.ContainsKey(key) || _queryStringParams[key] == null) { return null; }
            return _queryStringParams[key].ToString();
        }

        public void AddQueryStringParameter(string key, string value)
        {
            if (ReferenceEquals(_queryStringParams, null))
            {
                _queryStringParams = new Dictionary<string, string>();
            }

            _queryStringParams[key] = value;
        }
    }
}

﻿using System.Data.Common;

namespace Torneio.Web.Core
{
    public static class Application
    {
        private static bool _initialized = false;

        public static void Start()
        {
            if (_initialized) { return; } 
            _initialized = true;
        }
    }
}

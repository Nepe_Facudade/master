﻿

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Torneio.Web.Core.Models;

namespace Torneio.Web.Core.Domain
{
    #region Logic 
    public partial class Fighter
    {
        public List<Fighters> Get()
        {
            string responseJson = null;

            try
            {
                using (HttpClient httpClient = new HttpClient())
                {

                    httpClient.BaseAddress = new Uri("https://apidev-mbb.t-systems.com.br:8443/edgemicro_tsdev/torneioluta/api/competidores");

                    using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, ""))
                    {
                        request.Headers.Add("X-Api-Key", "29452a07-5ff9-4ad3-b472-c7243f548a33");
                        using (HttpResponseMessage response = httpClient.SendAsync(request).Result)
                        {
                            responseJson = response.Content.ReadAsStringAsync().Result;
                        }
                    }
                }
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
            }
            finally
            {
            }

            List<Fighters> data = JsonConvert.DeserializeObject<List<Fighters>>(responseJson);


            return data;
        }

        public List<Fighters> Start(List<Fighters> fighters)
        {
            List<Fighters> groupOne = new List<Fighters>();
            List<Fighters> groupTwo = new List<Fighters>();
            List<Fighters> groupThree = new List<Fighters>();
            List<Fighters> groupFour = new List<Fighters>();
            List<Fighters> groupQuarterfinals = new List<Fighters>();
            List<Fighters> groupThirdFourth = new List<Fighters>();
            List<Fighters> groupFinal = new List<Fighters>();


            fighters.Sort(delegate (Fighters x, Fighters y)
            {
                if (x.idade == null && y.idade == null) return 0;
                else if (x.idade == null) return -1;
                else if (y.idade == null) return 1;
                else return x.idade.CompareTo(y.idade);
            });

            for (var i = 0; i < 20; i++)
            {
                if (fighters[i].vitorias > 0)
                    fighters[i].porcentagem = Decimal.ToInt32(Decimal.Parse(fighters[i].vitorias.ToString()) / Decimal.Parse(fighters[i].lutas.ToString()) * 100);

                if (i < 5)
                    groupOne.Add(fighters[i]);
                else if (i < 10)
                    groupTwo.Add(fighters[i]);
                else if (i < 15)
                    groupThree.Add(fighters[i]);
                else
                    groupFour.Add(fighters[i]);
            }

            groupOne.Sort(delegate (Fighters x, Fighters y)
            {
                if (x.porcentagem == null && y.porcentagem == null) return 0;
                else if (x.porcentagem == null) return 1;
                else if (y.porcentagem == null) return -1;
                else if (x.porcentagem == y.porcentagem)
                {
                    if (y.artesMarciais.Length == x.artesMarciais.Length)
                        return y.lutas.CompareTo(x.lutas);
                    else return y.artesMarciais.Length.CompareTo(x.artesMarciais.Length);
                }
                else return y.porcentagem.CompareTo(x.porcentagem);
            });

            groupTwo.Sort(delegate (Fighters x, Fighters y)
            {
                if (x.porcentagem == null && y.porcentagem == null) return 0;
                else if (x.porcentagem == null) return 1;
                else if (y.porcentagem == null) return -1;
                else if (x.porcentagem == y.porcentagem)
                {
                    if (y.artesMarciais.Length == x.artesMarciais.Length)
                        return y.lutas.CompareTo(x.lutas);
                    else return y.artesMarciais.Length.CompareTo(x.artesMarciais.Length);
                }
                else return y.porcentagem.CompareTo(x.porcentagem);
            });

            groupThree.Sort(delegate (Fighters x, Fighters y)
            {
                if (x.porcentagem == null && y.porcentagem == null) return 0;
                else if (x.porcentagem == null) return 1;
                else if (y.porcentagem == null) return -1;
                else if (x.porcentagem == y.porcentagem)
                {
                    if (y.artesMarciais.Length == x.artesMarciais.Length)
                        return y.lutas.CompareTo(x.lutas);
                    else return y.artesMarciais.Length.CompareTo(x.artesMarciais.Length);
                }
                else return y.porcentagem.CompareTo(x.porcentagem);
            });


            groupFour.Sort(delegate (Fighters x, Fighters y)
            {
                if (x.porcentagem == null && y.porcentagem == null) return 0;
                else if (x.porcentagem == null) return 1;
                else if (y.porcentagem == null) return -1;
                else if (x.porcentagem == y.porcentagem)
                {
                    if (y.artesMarciais.Length == x.artesMarciais.Length)
                        return y.lutas.CompareTo(x.lutas);
                    else return y.artesMarciais.Length.CompareTo(x.artesMarciais.Length);
                }
                else return y.porcentagem.CompareTo(x.porcentagem);
            });


            // Quartas de final 
            if (groupOne[0].porcentagem == groupTwo[1].porcentagem)
            {
                if (groupOne[0].artesMarciais.Length == groupTwo[1].artesMarciais.Length)
                {
                    if (groupOne[0].lutas > groupTwo[1].lutas)
                        groupQuarterfinals.Add(groupOne[0]);
                    else
                        groupQuarterfinals.Add(groupTwo[1]);
                }
                else if (groupOne[0].artesMarciais.Length > groupTwo[1].artesMarciais.Length)
                    groupQuarterfinals.Add(groupOne[0]);
                else groupQuarterfinals.Add(groupTwo[1]);
            }
            else if (groupOne[0].porcentagem > groupTwo[1].porcentagem)
                groupQuarterfinals.Add(groupOne[0]);
            else groupQuarterfinals.Add(groupTwo[1]);

            if (groupOne[1].porcentagem == groupTwo[0].porcentagem)
            {
                if (groupOne[1].artesMarciais.Length == groupTwo[0].artesMarciais.Length)
                {
                    if (groupOne[1].lutas > groupTwo[0].lutas)
                        groupQuarterfinals.Add(groupOne[1]);
                    else
                        groupQuarterfinals.Add(groupTwo[0]);
                }
                else if (groupOne[1].artesMarciais.Length > groupTwo[0].artesMarciais.Length)
                    groupQuarterfinals.Add(groupOne[1]);
                else groupQuarterfinals.Add(groupTwo[0]);
            }
            else if (groupOne[1].porcentagem > groupTwo[0].porcentagem)
                groupQuarterfinals.Add(groupOne[1]);
            else groupQuarterfinals.Add(groupTwo[0]);


            if (groupThree[0].porcentagem == groupFour[1].porcentagem)
            {
                if (groupThree[0].artesMarciais.Length == groupFour[1].artesMarciais.Length)
                {
                    if (groupThree[0].lutas > groupFour[1].lutas)
                        groupQuarterfinals.Add(groupThree[0]);
                    else
                        groupQuarterfinals.Add(groupFour[1]);
                }
                else if (groupThree[0].artesMarciais.Length > groupFour[1].artesMarciais.Length)
                    groupQuarterfinals.Add(groupThree[0]);
                else groupQuarterfinals.Add(groupFour[1]);
            }
            else if (groupThree[0].porcentagem > groupFour[1].porcentagem)
                groupQuarterfinals.Add(groupThree[0]);
            else groupQuarterfinals.Add(groupFour[1]);

            if (groupThree[1].porcentagem == groupFour[0].porcentagem)
            {
                if (groupThree[1].artesMarciais.Length == groupFour[0].artesMarciais.Length)
                {
                    if (groupThree[1].lutas > groupFour[0].lutas)
                        groupQuarterfinals.Add(groupThree[1]);
                    else
                        groupQuarterfinals.Add(groupFour[0]);
                }
                else if (groupThree[1].artesMarciais.Length > groupFour[0].artesMarciais.Length)
                    groupQuarterfinals.Add(groupThree[1]);
                else groupQuarterfinals.Add(groupFour[0]);
            }
            else if (groupThree[1].porcentagem > groupFour[0].porcentagem)
                groupQuarterfinals.Add(groupThree[1]);
            else groupQuarterfinals.Add(groupFour[0]);



            //Semifinal
            if (groupQuarterfinals[0].porcentagem == groupQuarterfinals[1].porcentagem)
            {
                if (groupQuarterfinals[0].artesMarciais.Length == groupQuarterfinals[1].artesMarciais.Length)
                {
                    if (groupQuarterfinals[0].lutas > groupQuarterfinals[1].lutas)
                    {
                        groupFinal.Add(groupQuarterfinals[0]);
                        groupThirdFourth.Add(groupQuarterfinals[1]);
                    }
                    else
                    {
                        groupFinal.Add(groupQuarterfinals[1]);
                        groupThirdFourth.Add(groupQuarterfinals[0]);
                    }
                }
                else if (groupQuarterfinals[0].artesMarciais.Length > groupQuarterfinals[1].artesMarciais.Length)
                {
                    groupFinal.Add(groupQuarterfinals[0]);
                    groupThirdFourth.Add(groupQuarterfinals[1]);
                }
                else
                {
                    groupFinal.Add(groupQuarterfinals[1]);
                    groupThirdFourth.Add(groupQuarterfinals[0]);

                }
            }
            else if (groupQuarterfinals[0].porcentagem > groupQuarterfinals[1].porcentagem)
            {
                groupFinal.Add(groupQuarterfinals[0]);
                groupThirdFourth.Add(groupQuarterfinals[1]);
            }
            else
            {
                groupFinal.Add(groupQuarterfinals[1]);
                groupThirdFourth.Add(groupQuarterfinals[0]);
            }


            if (groupQuarterfinals[2].porcentagem == groupQuarterfinals[3].porcentagem)
            {
                if (groupQuarterfinals[2].artesMarciais.Length == groupQuarterfinals[3].artesMarciais.Length)
                {
                    if (groupQuarterfinals[2].lutas > groupQuarterfinals[3].lutas)
                    {
                        groupFinal.Add(groupQuarterfinals[2]);
                        groupThirdFourth.Add(groupQuarterfinals[3]);
                    }
                    else
                    {
                        groupFinal.Add(groupQuarterfinals[3]);
                        groupThirdFourth.Add(groupQuarterfinals[2]);
                    }
                }
                else if (groupQuarterfinals[2].artesMarciais.Length > groupQuarterfinals[3].artesMarciais.Length)
                {
                    groupFinal.Add(groupQuarterfinals[2]);
                    groupThirdFourth.Add(groupQuarterfinals[3]);
                }
                else
                {
                    groupFinal.Add(groupQuarterfinals[3]);
                    groupThirdFourth.Add(groupQuarterfinals[2]);
                }
            }
            else if (groupQuarterfinals[2].porcentagem > groupQuarterfinals[3].porcentagem)
            {
                groupFinal.Add(groupQuarterfinals[2]);
                groupThirdFourth.Add(groupQuarterfinals[3]);
            }
            else
            {
                groupFinal.Add(groupQuarterfinals[3]);
                groupThirdFourth.Add(groupQuarterfinals[2]);
            }

            //Terceiro e Quarto lugar
            groupThirdFourth.Sort(delegate (Fighters x, Fighters y)
            {
                if (x.porcentagem == null && y.porcentagem == null) return 0;
                else if (x.porcentagem == null) return 1;
                else if (y.porcentagem == null) return -1;
                else if (x.porcentagem == y.porcentagem)
                {
                    if (y.artesMarciais.Length == x.artesMarciais.Length)
                        return y.lutas.CompareTo(x.lutas);
                    else return y.artesMarciais.Length.CompareTo(x.artesMarciais.Length);
                }
                else return y.porcentagem.CompareTo(x.porcentagem);
            });

            //final
            groupFinal.Sort(delegate (Fighters x, Fighters y)
            {
                if (x.porcentagem == null && y.porcentagem == null) return 0;
                else if (x.porcentagem == null) return 1;
                else if (y.porcentagem == null) return -1;
                else if (x.porcentagem == y.porcentagem)
                {
                    if (y.artesMarciais.Length == x.artesMarciais.Length)
                        return y.lutas.CompareTo(x.lutas);
                    else return y.artesMarciais.Length.CompareTo(x.artesMarciais.Length);
                }
                else return y.porcentagem.CompareTo(x.porcentagem);
            });

            fighters = new List<Fighters>();

            fighters.AddRange(groupFinal);
            fighters.AddRange(groupThirdFourth);

            return fighters;
        }
    }

    #endregion
}

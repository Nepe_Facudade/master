﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks; 

namespace Torneio.Web.Controllers
{
    using Core;
    using Core.Http;
    using Core.Domain;
    using Core.Models;
    using System.Collections.Generic;

    [Route("api/[controller]")]
    [ApiController]
    public class FighterController : ControllerBase
    {
          

        [HttpPost("GetFighter")]
        public async Task<ActionResult> GetFighter()
        {
            return await Proxy.ResolveJSON(this.Request, null, true, (JsonEnvelope env, out bool applyEnvelope) =>
            {
                applyEnvelope = true;

                List<Fighters> fighters = new Fighter().Get();

                env.WarningLevel = WarningLevel.Success;

                return fighters;
            });
        }

        [HttpPost("Start")]
        public async Task<ActionResult> Start()
        {
            return await Proxy.ResolveJSON(this.Request, null, true, (JsonEnvelope env, out bool applyEnvelope) =>
            {
                applyEnvelope = true;

                List<Fighters> fighters =  env.ToJson<List<Fighters>>();

                List<Fighters> winners = new Fighter().Start(fighters);

                env.WarningLevel = WarningLevel.Success;

                return winners;
            });
        }

    }
}

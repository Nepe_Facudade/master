﻿using System;

namespace Torneio.Web
{
    internal class HttpLog  
    {
        public void Error(string message, Exception error = null)
        {
            Console.WriteLine(message);
        }

        public void Info(string message)
        {
            Console.WriteLine(message);
        }

        public void Warning(string message)
        {
            Console.WriteLine(message);
        }

        public static HttpLog Start()
        {
            return new HttpLog();
        }
    }
}

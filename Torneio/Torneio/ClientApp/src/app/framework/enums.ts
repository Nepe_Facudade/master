export enum WarningLevel {
    Info = 0,
    Success = 1,
    Warning = 2,
    Error = 3
}

export enum LivenessType {
    Active = 1,
    Passive = 2,
    Both = 3
}
import { isNull } from "./core";

export interface IQueryFilter {
    sequence: number,
    logicOperator: "AND" | "OR",
    fieldName: string,
    operator: "=" | "!=" | "<>" | "<" | ">" | "like" | "contains" | "isnull" | "isnotnull",
    value: any
}

export class QueryFilter {
    private _sequence: number = 0;
    private _items: Array<IQueryFilter> = null;

    public get items(): Array<IQueryFilter> {
        if (isNull(this._items)) { this._items = new Array<IQueryFilter>(); }
        return this._items;
    }

    constructor(fieldName: string, value: any, operator: "=" | "!=" | "<>" | "<" | ">" | "like" | "contains" | "isnull" | "isnotnull" = "=") {
        this._sequence++;
        this.items.push({
            sequence: this._sequence,
            logicOperator: "AND",
            fieldName: fieldName,
            operator: operator,
            value: value
        });
    }

    public and(fieldName: string, value: any, operator: "=" | "!=" | "<>" | "<" | ">" | "like" | "contains" | "isnull" | "isnotnull" = "="): QueryFilter {
        this._sequence++;
        this.items.push({
            sequence: this._sequence,
            logicOperator: "AND",
            fieldName: fieldName,
            operator: operator,
            value: value
        });
        return this;
    }

    public or(fieldName: string, value: any, operator: "=" | "!=" | "<>" | "<" | ">" | "like" | "contains" | "isnull" | "isnotnull" = "="): QueryFilter {
        this._sequence++;
        this.items.push({
            sequence: this._sequence,
            logicOperator: "OR",
            fieldName: fieldName,
            operator: operator,
            value: value
        });
        return this;
    }

    public parseToString(): string {
        const notation: string = JSON.stringify(this.items);
        return `queryFilter=${encodeURIComponent(btoa(notation))}`;
    }
}
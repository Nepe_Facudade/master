import { IValidator, IEnvelope } from "./interfaces";
import { WarningLevel } from "./enums";
import { Queryable } from "./queryable";

declare function moment(value: string | Date): any;

declare var $: any;

export function isNull(value: any): boolean {
    return ((value == null) || (value == undefined));
}

export function isNullOrEmpty(value: any): boolean {
    if (isNull(value)) { return true; }

    if (typeof value == "string") {
        return (value.length == 0);
    } else if (value instanceof Array) {
        return (value.length == 0);
    }

    return false;
}

export function isNullOrBlank(value: string): boolean {
    if (isNullOrEmpty(value)) { return true; }
    if (typeof value == "string") {
        return (value.trim().length == 0);
    } else {
        return false;
    }
}

export function replaceAll(text: string, oldValue: string, newValue: string): string {
    while (text.indexOf(oldValue) >= 0) {
        text = text.replace(oldValue, newValue);
    }
    return text;
}

export function generateNewGUID(): string {
    var s4 = function () {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    };
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

export function query<T>(collection: Array<T>): Queryable<T> {
    return new Queryable<T>(collection);
}

export function randomize(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function formatDate(value: string | Date, format: string) {
    return moment(value).format(format);
}

export function keepNumbers(input: string): string {
    if (isNullOrBlank(input)) { return input; }
    return input.replace(/\D/g, '');
}

export function focus(elementId: string): void {
    $("#" + elementId).focus();
    setTimeout(() => {
        if (document.activeElement.id != elementId) {
            $("#" + elementId).focus();
        }
    }, 1000);
}

export class ValidationContext {
    private innerValidator: IValidator = null;
    public isValid: boolean = true;

    constructor(validator: IValidator) {
        this.innerValidator = validator;
    }

    public requestField(fieldName: string, fieldValue: string | number): void {
        if (!this.innerValidator.requestField(fieldName, fieldValue)) {
            this.isValid = false;
        }
    }

    public verifyDate(fieldName: string, fieldValue: string): void {
        if (!this.innerValidator.verifyDate(fieldName, fieldValue)) {
            this.isValid = false;
        }
    }

    public compare(fieldNameA: string, fieldValueA: string, fieldNameB: string, fieldValueB: string): void {
        if (!this.innerValidator.compare(fieldNameA, fieldValueA, fieldNameB, fieldValueB)) {
            this.isValid = false;
        }
    }
}

export class Envelope<T> implements IEnvelope<T> {
    public content: T = null;
    public warningLevel: WarningLevel = null;
    public message: string;
    public pageSize: number;
    public pageIndex: number;
    public pageCount: number;
    public totalCount: number;

    constructor(data: T) {
        this.content = data;
    }
}

export class PropertyMap {
    public label: string = null;
    public propertyName: string = null;
    public propertyType: "string" | "number" | "date" = "string";

    public static create(label: string, propertyName: string, propertyType: "string" | "number" | "date" = "string"): PropertyMap {
        return {
            "label": label,
            "propertyName": propertyName,
            "propertyType": propertyType
        };
    }
}

export function parseToDate(inputValue: string): Date {
    if (isNullOrBlank(inputValue)) { return null; }
    const splitValues: string[] = inputValue.split(/[^0-9]/);
    const isoFormat: RegExpMatchArray = inputValue.match(/\d{4}-[01]\d-[0-3]\d/);
    const currentDateTime: Date = new Date();
    if (splitValues.length == 0) { return null; }
    const parts: number[] = (new Queryable(splitValues)).select((sv) => { return parseInt(sv); });
    if (isoFormat) {
        if (parts.length <= 3) {
            while (parts.length < 3) {
                parts.push(1);
            }
            return new Date(parts[0], parts[1] - 1, parts[2]);
        }
        while (parts.length < 6) {
            parts.push(0);
        }
        if (parts[0] > 2100) { parts[0] = 2100; }
        if (parts[1] > 12) { parts[1] = 12; }
        if (parts[2] > 31) { parts[2] = 31; }
        if (parts[3] > 23) { parts[3] = 23; }
        if (parts[4] > 59) { parts[4] = 59; }
        if (parts[5] > 59) { parts[5] = 59; }
        if (parts[0] < 1900) { parts[0] = 1900; }
        if (parts[1] < 0) { parts[1] = 0; }
        if (parts[2] < 1) { parts[2] = 1; }
        if (parts[3] < 0) { parts[3] = 0; }
        if (parts[4] < 0) { parts[4] = 0; }
        if (parts[5] < 0) { parts[5] = 0; }
        return new Date(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]);
    } else {
        if (parts.length < 2) {
            parts.push(currentDateTime.getMonth());
        }
        if (parts.length < 3) {
            parts.push(currentDateTime.getFullYear());
        }
        if (parts.length === 3) {
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }
        if (parts.length < 4) {
            parts.push(currentDateTime.getHours());
        }
        if (parts.length < 5) {
            parts.push(currentDateTime.getMinutes());
        }
        if (parts.length < 6) {
            parts.push(currentDateTime.getSeconds());
        }
        if (parts[2] > 2100) { parts[0] = 2100; }
        if (parts[1] > 12) { parts[1] = 12; }
        if (parts[0] > 31) { parts[2] = 31; }
        if (parts[3] > 23) { parts[3] = 23; }
        if (parts[4] > 59) { parts[4] = 59; }
        if (parts[5] > 59) { parts[5] = 59; }
        if (parts[2] < 1900) { parts[0] = 1900; }
        if (parts[1] < 0) { parts[1] = 0; }
        if (parts[0] < 1) { parts[2] = 1; }
        if (parts[3] < 0) { parts[3] = 0; }
        if (parts[4] < 0) { parts[4] = 0; }
        if (parts[5] < 0) { parts[5] = 0; }
        return new Date(parts[2], parts[1] - 1, parts[0], parts[3], parts[4], parts[5]);
    }
}
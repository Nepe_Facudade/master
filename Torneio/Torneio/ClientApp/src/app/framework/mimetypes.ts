let MimeTypes = [
    {
        "name": ".doc"
    },
    {
        "name": ".docx"
    },
    {
        "name": ".html"
    },
    {
        "name": ".pdf"
    },
    {
        "name": ".ppt"
    },
    {
        "name": ".pptx"
    },
    {
        "name": ".txt"
    },
    {
        "name": ".xls"
    },
    {
        "name": ".xlsx"
    },
    {
        "name": ".zip"
    },
]
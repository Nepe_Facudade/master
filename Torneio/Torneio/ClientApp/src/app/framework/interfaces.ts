import { ActivatedRoute, Params } from "@angular/router";

import { WarningLevel } from "./enums";

import { Message } from './message';

export interface IAppRouteNavigationHandler {
    currentRoute: ActivatedRoute,
    hasQueryParameter(key: string),
    changeQueryString(parameters: Params): void,
    getQueryParameter(key: string): string,
    redirect(url: string): void,
    goTo(url: string): void,
    changeRoute(url: string, parameters?: Params): void,
    goBack(): void
}

export interface IAppMessageHandler {
    add(msg: Message): void,
    clear(): void;
}

export interface IValidator {
    requestField(fieldName: string, fieldValue: string | number): boolean,
    verifyDate(fieldName: string, fieldValue: string): boolean,
    compare(fieldNameA: string, fieldValueA: string, fieldNameB: string, fieldValueB: string): boolean,
}

export interface IAppContext extends IValidator {
    messages: IAppMessageHandler,
    navigation: IAppRouteNavigationHandler,
    operationGroup: string,
    messageContainer: string,
    readonly current: any
}

export interface IKeyValuePair {
    key: string,
    value: any
}

export interface IEnvelope<T> {
    content: T,
    warningLevel: WarningLevel,
    message: string,
    pageSize: number,
    pageIndex: number,
    pageCount: number,
    totalCount: number
}
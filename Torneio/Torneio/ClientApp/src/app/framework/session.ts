export class Session {
    public static getString(key: string, defaultValue?: string): string {
        let result: string = sessionStorage.getItem(key);
        return (result || defaultValue);
    }

    public static setString(key: string, value: string): void {
        sessionStorage.setItem(key, value);
    }

    public static clear() {
        sessionStorage.clear();
    }
}
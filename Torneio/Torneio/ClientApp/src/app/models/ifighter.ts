export interface IFighter {
  id: number,
  nome: string, 
  idade: number,
  artesMarciais: [],
  lutas: number,
  derrotas: number,
  vitorias: number,
}

import { Component } from '@angular/core';
import { Storage } from '../framework/storage';

@Component({
  selector: 'app-podium-component',
  templateUrl: './podium.component.html'
})
export class PodiumComponent {


  public _firstPlace: string = null;
  public _secondPlace: string = null;
  public _thirdPlace: string = null;

  constructor() {
  }

  public ngOnInit() {
    this._firstPlace = Storage.getString("firstPlace");
    this._secondPlace = Storage.getString("secondPlace");
    this._thirdPlace = Storage.getString("thirdPlace");

  }

  public ngAfterViewInit() { 
  }
}

import { Component, OnInit, AfterViewInit } from '@angular/core';
import { WarningLevel } from '../framework/enums';
import { Storage } from '../framework/storage';
import { IFighter } from '../models/ifighter';
import { FighterService } from '../services/business/fighter.service';
import * as FW from "../framework/core";
import { ContextService } from '../services/application/context.service';
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit, AfterViewInit {

  public fighters: IFighter = null;
  public fighterSelection: IFighter[] = [];
  public winners: IFighter = null;

  constructor(private srcFighter: FighterService, private context: ContextService) {
  }

  public ngOnInit() {
    this.clear();
  }

  public ngAfterViewInit() {
    this.search();
  }


  public search() {

    this.srcFighter.getFighter().subscribe(
      (result) => {
        if (result.warningLevel == WarningLevel.Success && result.content != null) {
          this.fighters = result.content;
        }
      })
  }

  public clear() {
    this.fighters = null;
  }

  public select(fighter: IFighter, id:any) {
    
    let checkbox = $('#checkbox' + id);
    if (checkbox.is(":checked")) { 
      if (this.fighterSelection.length >= 20) {
        alert("Permitido apenas 20 lutadores por luta, por favor inicie as lutas");
        checkbox.prop('checked', false);
        return;
      }

      this.fighterSelection.push(fighter);
    } else {  
      var index = this.fighterSelection.indexOf(fighter);
      this.fighterSelection.splice(index, 1); 
      }
  }

  public start() {
    this.srcFighter.startFighter(this.fighterSelection).subscribe(
      (result) => {
        if (result.warningLevel == WarningLevel.Success && result.content != null) {
          this.winners = result.content;

          if (!FW.isNullOrEmpty(this.winners[0]))
            Storage.setString("firstPlace", this.winners[0].nome);
          if (!FW.isNullOrEmpty(this.winners[1]))
            Storage.setString("secondPlace", this.winners[1].nome);
          if (!FW.isNullOrEmpty(this.winners[2]))
            Storage.setString("thirdPlace", this.winners[2].nome);

          this.context.navigation.changeRoute("/podium");
        }
      })
  }

}

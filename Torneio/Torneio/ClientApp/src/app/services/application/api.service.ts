import { Injectable, Inject } from "@angular/core"; 
import { HttpClient, HttpHeaders, } from '@angular/common/http'; 
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IAppContext, IEnvelope } from "../../framework/interfaces";
import { HttpErrorHandleDelegate, HttpErrorHandler } from "./httperrorhandler.service";
  
const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root',
})
export class APIService { 
   
    private baseApiUrl: string = null;

    constructor(private httpClient: HttpClient, @Inject('BASE_URL') baseAppUrl: string) {
        console.log("Creating API service");  
        this.baseApiUrl = baseAppUrl + "api";
    }

    public get<T>(ctx: IAppContext, url: string): Observable<T> {
        return this.httpClient.get<T>(this.baseApiUrl + url).
            pipe(
                catchError(this.handleHttpError(ctx, url, null))
            );
    }

    public post<T>(ctx: IAppContext, url: string, postEnvelope: IEnvelope<T>): Observable<IEnvelope<T>> {
        return this.httpClient.post<IEnvelope<T>>(this.baseApiUrl + url, postEnvelope, httpOptions).
            pipe(
                catchError(this.handleHttpError(ctx, url, postEnvelope))
            );
  }

  public handleHttpError(ctx, url, postEnvelope) {

    return null;
  }
}

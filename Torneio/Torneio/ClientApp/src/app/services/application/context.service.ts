import { Injectable } from "@angular/core";
import { WarningLevel } from "../../framework/enums";
import { IAppContext, IValidator } from "../../framework/interfaces";
import { Message, MessageBehavior } from "../../framework/message";
import { MessageService } from "./message.service";
import { NavigationService } from "./navigation.service";
 
declare var moment: any;

@Injectable({
    providedIn: 'root',
})
export class ContextService implements IAppContext, IValidator {
    public get operationGroup(): string { return "Generic"; }
    public get messageContainer(): string { return "content-top" }

    private static _context: any = null;
    private static _isolatedCtx: IAppContext = null;

    public get current(): any {
        if (ContextService._context == null) {
            ContextService._context = new Object();
        }
        return ContextService._context;
    }

    constructor(public messages: MessageService,  public navigation: NavigationService ) {
    }

    public join(group: string): IAppContext {
      if (ContextService._isolatedCtx != null) {   
            const result: IAppContext = {
                current: ContextService._isolatedCtx.current,
                messageContainer: ContextService._isolatedCtx.messageContainer,
                messages: ContextService._isolatedCtx.messages,
                navigation: ContextService._isolatedCtx.navigation,
                operationGroup: group,
                compare: this.compare,
                requestField: this.requestField,
                verifyDate: this.verifyDate
            };
            ContextService._isolatedCtx = null;
            return result;
        } else {
            return {
                current: this.current,
                messageContainer: this.messageContainer,
                messages: this.messages,
                navigation: this.navigation,
                operationGroup: group,
                compare: this.compare,
                requestField: this.requestField,
                verifyDate: this.verifyDate
            };
        }
    }

    public isolate(msgContainer: string, ctx?: any): void {
        ContextService._isolatedCtx = {
            current: ctx,
            messageContainer: msgContainer,
            messages: this.messages,
            navigation: this.navigation,
            operationGroup: this.operationGroup,
            compare: this.compare,
            requestField: this.requestField,
            verifyDate: this.verifyDate
        };
    }

    public requestField(fieldName: string, fieldValue: string | number): boolean {
        if (typeof (fieldValue) == "number") {
          if (fieldValue == null) {
                const validationMsg: Message = new Message(this.messageContainer, `O campo ${fieldName} é obrigatório.`, null, WarningLevel.Warning, MessageBehavior.Toast);
                this.messages.add(validationMsg);
                return false;
            }
        } else if (fieldValue == null || fieldValue =="") {
            const validationMsg: Message = new Message(this.messageContainer, `O campo ${fieldName} é obrigatório.`, null, WarningLevel.Warning, MessageBehavior.Toast);
            this.messages.add(validationMsg);
            return false;
        }

        return true;
    }

    public verifyDate(fieldName: string, fieldValue: string): boolean {
      if (fieldValue == null || fieldValue == "")  { return true; }
        let testingDate = moment(fieldValue);

        if (!testingDate.isValid()) {
            const validationMsg: Message = new Message(this.messageContainer, `O campo ${fieldName} deve conter uma data válida.`, null, WarningLevel.Warning, MessageBehavior.Toast);
            this.messages.add(validationMsg);
            return false;
        }

        return true;
    }

    public compare(fieldNameA: string, fieldValueA: string, fieldNameB: string, fieldValueB: string): boolean {
        if (fieldValueA !== fieldValueB) {
            const validationMsg: Message = new Message(this.messageContainer, `O campo ${fieldNameB} não confere com o campo ${fieldNameA}.`, null, WarningLevel.Warning, MessageBehavior.Toast);
            this.messages.add(validationMsg);
            return false;
        }

        return true;
    }
}

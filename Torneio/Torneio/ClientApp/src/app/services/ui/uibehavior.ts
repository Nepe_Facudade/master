 
declare var $: any;

declare function hideElement(id: string): any;
declare function fadeInElement(id: string): any;
declare function showModal(id: string): any;
declare function hideModal(id: string): any;

declare function displayToast(title: string, message: string, type: string): any;

export class UIBehavior {
   

    public static disposeView(): void {
        $("#divViewContainer > :not(router-outlet)").each(function () {
            $(this).remove();
        });
    }
   
    public static hide(id: string): void {
        hideElement(id);
    }

    public static fadeIn(id: string): void {
        fadeInElement(id);
    }

    public static openModal(id: string, dismissCallback?: () => any): void {
        showModal(id).off('hidden.bs.modal').on('hidden.bs.modal', function () {
            if (dismissCallback) { dismissCallback(); }
        });
    }

    public static closeModal(id: string): void {
        hideModal(id);
    }

    public static notify(title: string, message: string, type: "info" | "success" | "warning" | "error"): void {
        displayToast(title, message, type);
    }

    public static alert(title: string, message: string, type: "info" | "success" | "warning" | "error"): Promise<any> {
      return null;
    }

    public static confirm(title: string, message: string): Promise<any> {
      return null;
    }
}

import { Injectable } from "@angular/core";
import { Observable, of } from 'rxjs';
import { IAppContext } from "../../framework/interfaces";
import { IFighter } from "../../models/ifighter";
import { APIService } from "../application/api.service";
import { ContextService } from "../application/context.service";
import * as FW from "../../framework/core";

@Injectable({
  providedIn: 'root',
})
export class FighterService {

  public get serviceName(): string { return "Fighter" };

  constructor(public context: ContextService, private api: APIService) {
  }

  public getFighter(): Observable<any> {
    const ctx: IAppContext = this.context.join(this.serviceName);

    return this.api.post(ctx, "/fighter/GetFighter", null);
  }

  public startFighter(fighters: IFighter[]): Observable<any> {
    const ctx: IAppContext = this.context.join(this.serviceName);

    const postEnvelope = new FW.Envelope(fighters);

    return this.api.post(ctx, "/fighter/Start", postEnvelope);
  } 

}
